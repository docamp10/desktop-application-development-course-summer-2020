# Downloads and environment setup

* **Download Video** - [Here](https://www.dropbox.com/s/wz63gvw4ghebbzb/course_description_and_first_lecture.mp4?dl=0)

## Developer Getting Started

The following instructions guide developers through setting up an environment to develop desktop applications using Java, the IntelliJ IDEA and Scenebuilder.

### Prerequisites

Before attempting the instructions below, there are several applications you will need installed on your
local machine.

Pay specific attention to **version numbers** and install the versions listed. For each prerequisite, choose
the appropriate download for your environment, download and install it on your local machine.


#### Java JDK and JRE Version 8u151

Link: [Oracle Site - Java JRE](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) - download *jre-8u151-windows-x64.exe*

Link: [Oracle Site - Java JDK](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) - download *jdk-8u151-windows-i586.exe*

After a succesful installation, you will have a Java folder in *Program Files* that must look like this:  

![pic](correct_installation.png)

#### JavaFX Scene Builder v2.0

There are two different versions of Scenebuilder. For some specific projects' dependencies, you may have to install one or the other.

Link: [Oracle Site - Scene Builder 2.0](https://www.oracle.com/java/technologies/javafxscenebuilder-1x-archive-downloads.html)

Link: [Gluon Site - Scene Builder 2.0](https://gluonhq.com/products/scene-builder/#download)

#### IntelliJ IDEA Community Edition (CE)

Link: [Jetbrains - IntelliJ IDEA CE](https://www.jetbrains.com/idea/download/index.html)

#### Git

Link: [Git - Downloads](https://git-scm.com/downloads)

Installation instructions: [Git - Installing Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


### Additional libraries for Scenebuilder

If Scenebuilder is not opening successfully some of your fxml files, it must be due to dependencies to libraries inside Scenebuilder. A common one is JFoenix.

Link: [JFoenix repository](https://github.com/jfoenixadmin/JFoenix):  
Download the JAR file corresponding to your Java version. Then, open Scenebuilder's JAR manager and import the downloaded JAR file.  

![pic](jfoenix_jar_file.png)




#### SourceTree

Git as a version control system is quite complex and has a long list of commands to be used by terminal. In order to make things easier, we use [SourceTree](https://www.sourcetreeapp.com/). However, If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).




## Deployment

TODO: Add additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [JavaFX](https://openjfx.io/) - JavaFX
* [AfterburnerFX](http://afterburner.adam-bien.com/) - Minimalistic MVP Framework


## Author

* **Daniel Ocampo** - [email](daniel.ocampomillan@my.utsa.edu)
