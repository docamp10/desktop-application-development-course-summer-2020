# Desktop Application Development Course - Assignment 1

In this assignment you will learn how to work with Buffer readers/writers in Java.   

## Objectives:  
- Implement a Buffer reader/writer using Java.  
- Modify existing source code.  
- Understand the software arquitecture of a real, business size application that uses Afterburner, Git and Mave as part of its structure.  
- Pull/push changes to a repository.  

---

## Description of the task


1.Checkout the last commit from the SMart|DT application. You can do this by using SourceTree, or using the following command  (**make sure you download from the *version-1-1-develop* branch**):

```
git clone -b version-1-1-develop https://docamp10@bitbucket.org/quj690/smart-desktop.git
```  

Once you are able to run, you must obtain the following application. *Note, version number 1.1.1*.    
...
![alt](https://bitbucket.org/docamp10/desktop-application-development-course-summer-2020/raw/d47f6792b175beff6f16c72636fc895afe28ce0f/extra_assignment_1/images/smartdt.PNG)    
...

2. The goal of this assignment is to come up with an idea to visualize clearly the content of the Tabular File displayed when pressing the *View Text* button on the geometry screen, as seen in the following picture. Examples of how this could be implemented can be found [here](https://stackoverflow.com/questions/2745206/output-in-a-table-format-in-javas-system-out).    
...
![pic](images/tabular_view_text.png)    
...

To do this, find the corresponding package in the project structure. There, you'll see the package corresponding to the Statistical distribution which is instantiated for Initial crack size within the geometry presentation/screen.  
...
![pic](images/project_tree.png)
...    

- Just follow the action executed when the *View Text* button is pressed.  

- One example of a Tabular file to be used for testing can be found within the *extra_assignment_1* folder.


---  
## Author

* **Daniel Ocampo** - [email](daniel.ocampomillan@my.utsa.edu)

