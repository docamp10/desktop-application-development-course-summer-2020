# Core Java Language 

Java is a high level, modern programming language designed in the early 1990s by Sun Microsystems, and currently owned by Oracle.  
 
Java is Platform Independent, which means that you only need to write the program once to be able to run it on a number of different platforms! 
Java is portable, robust, and dynamic, with the ability to fit the needs of virtually any type of application.


* **Download Video 1** - [Here](https://www.dropbox.com/s/eabotwllo2tuemx/java_fundamentals.mp4?dl=0)  

* **Download Video 2** - [Here](https://www.dropbox.com/s/0etgzcfbeo1zkx0/oop.mp4?dl=0)


## Basic Concepts  

### First Java Program  

```
class MyClass {
	public static void main(String[ ] args) {
    	System.out.println("Hello World");
	}
}
```

### The *public void static method*  

- **public**: anyone can access it
- **static**: method can be run without creating an instance of the class containing the main method
- **void**: method doesn't return any value
- **main**: the name of the method

#### Variables
```
String name = "Steven";
```
```
int age = 27;
```
```
double weight = 154.3;
```
```
class MyClass {
	public static void main(String[ ] args) {
		String name = "Steven";
		int age = 10;
		double weight = 154.91;
		boolean online = true;
	}
}
```

#### Conditionals and Loops
##### *if* statement.

```
if (condition) {
   //Executes when the condition is true
}
```
Example  
```
int x = 7;
if(x < 42) {
   System.out.println("Hi");
}
```
##### *if*/*else* statement
```
int age = 19;

if (age < 21) {
   System.out.println("Can't enter");
} else { 
   System.out.println("Welcome!");
}
```
##### *for* Loop
```
for (initialization; condition; increment/decrement) {
   statement(s)
}
```
Example  
```
for(int x = 1; x <=5; x++) {
  System.out.println(x);
}
```

##### *while* Loop
```
int x = 3;

while(x > 0) {
   System.out.println(x);
   x--;
}
```

Example
```
int x = 6;

while( x < 7 ){
  System.out.println(x);
  x++;
}
System.out.println("Loop ended");
```

---
### Object Oriented Programming OOP  
#### Classes  
| Attributes | Behavior |
| --------|---------|
| name   | walk  |
| height | run   |
| weight | jump  |
| gender | speak |
| age    | sleep |

```
public class Animal {
	String name = "Ruffo";
	void bark() {
		System.out.println("Woof-Woof");
	}
}
```

#### Methods
```
class MyClass {
	static void sayHello() {
		System.out.println("Hello World!");
	}

	public static void main(String[ ] args) {
		sayHello();
		sayHello();
		sayHello();
	}
}
```

#### Method Parameters
```
class MyClass {
	static void sayHello(String name) {
		System.out.println("Hello " + name);
	}

	public static void main(String[ ] args) {
		sayHello("David");
		sayHello("Amy");
	}
}
```

#### Method Return Types
```
static int sum(int val1, int val2) {
	return val1 + val2;
}
```
Example  
```
class MyClass {

	static int sum(int val1, int val2) {
		return val1 + val2;
	}

	public static void main(String[ ] args) {
		int x = sum(2, 5);
		System.out.println(x);
	}
}
```

#### Access Modifiers
```
public static void main(String[ ] args)
```  
Example  
```
public class Vehicle {
	private int maxSpeed;
	private int wheels;
	private String color;
	private double fuelCapacity;

	public void horn() {
		System.out.println("Beep!");
	}
}
```

#### Getters & Setters  
```
public class Vehicle {
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String c) {
		this.color = c;
	}
}
```



*Some examples are taken from [sololearn.com](sololearn.com)*


