# Naming conventions in Java
---
The following key rules must be followed by every identifier, in general.  

- No white spaces.
- Names must NOT contain special characters like ($, &, or _).  

---

### Now, for specific identifiers:

## Classes:  
- Must start with upper case letter.
- Must be a noun, e.g. *Car*, *Button*, *Person*, *WeibullDistribution*, etc.
- Use appropiate words instead of acronyms.  

Example  
```
public class Car {  
	//code snippet  
}  
```

## Interfaces  
- Must start with uppercase letter.  
- It should be an adjective such as *Runnable*, *Remote*, *ActionListener*.  
- Use appropriate words, instead of acronyms.  

Example  
```
Interface Printable {  
	//code snippet  
}  
```

## Methods  
- Must start with lowercase letter.  
- Must  be a verb such as *main()*, *print()*, *doSomething()*.  
- If the name contains multiple words, start it with a lowercase letter followed by an uppercase letter such as *computeIntegral()*.  

Example  
```
 class Employee {  
	//method  
	void computeResults() {  
		//code snippet  
	}  
}    
```

## Variables
- Must start with lowercase letter.  
- If the name contains multiple words, start it with the lowercase letter followed by an uppercase letter such as *firstName*, *lastName*.  
- Avoid using one-character variables such as *x*, *y*, *z*.  

Example  

```
class Distribution {  
	// variables
	int numberOfSamples;
	String name;
}    
```  

## Packages  
- Must have only lowercase letters.
- If the name contains multiple words, must be separated by dots (.) such as *java.util, java.lang*.  

Example  

```
package java.lang.math3; //package  

class Car {  
	//code snippet  
}      
```  

## Constants  
- Must be uppercase letters such as RED, YELLOW.
- If the name contains multiple words, must be separated by an underscore(_) such as MAX_PRIORITY.
- May contain digits but not as the first letter.  

Example  

```
class Student {  
	//constant  
	 static final int MAX_AGE = 18;  
	//code snippet  
}  
```  

---  
Note, Java follows **camel-case** syntax for naming classes, interfaces, methods and variables. Which means, if the name contains two words, then the second word must start with uppercase letter, such as *actionPerformed()*, *firstName*, *ActionEvent*, *ActionListener*, etc.  

---
Taken from [javatpoint.com](javatpoint.com).

---

## Author

* **Daniel Ocampo** - [email](daniel.ocampomillan@my.utsa.edu)
