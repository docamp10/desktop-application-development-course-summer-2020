# Desktop Application Development Course - Summer 2020

This is Desktop Applications/Software Development Course for non computer scientists.

*In this video course you will learn how to develop desktop applications for both Mac and Windows machines using JavaFX. We will cover everything from creating a basic window, to creating bussiness size applications using advanced frameworks such as afterburner.*

---

## List of topics

1. Downloads and environment setup (1 video).  


### Core Java Language (1 videos)

2. OOP, methods, class attributes, access modifiers, getters and setters.  
~ Detailed document with description of naming conventions (1 document)

---

### Desktop App Development with JavaFX (9 videos)

3. Creating a basic window, application structure, html/fxml basics.
4. Handling user events.
5. Communicating between windows.
6. Working with layouts.
7. Events listeners.
8. Comboboxes, Textfields Buttons and control items.
9. Working with TableViews.
10. Properties and bindings.
---

### Git basics (5 videos)

Git as a version control system is quite complex and has a long list of commands to be used by terminal. In order to make things easier, we use [SourceTree](https://www.sourcetreeapp.com/). However, If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

11. Installing git
12. How git works
13. Creating repositories, staging files, making commits
14. Undoing things
15. Branches


### Maven basics (5 videos)

16. Introduction, installation and setup
17. Creating First Maven project
18. Understanding pom.xml
19. Introduction to build lifecycle
20. Managing dependencies

### Afterburner (1 video)
21. Model View Presenter protocol MVP 

## Author

* **Daniel Ocampo** - [email](daniel.ocampomillan@my.utsa.edu)

