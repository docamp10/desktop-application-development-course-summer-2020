# Introduction to Git

* **Download Video 1** - [Here](https://www.dropbox.com/s/b5rifwfd0mjxc20/3_git_basics.wmv?dl=0)

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.  

Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like Subversion, CVS, Perforce, and ClearCase with features like cheap local branching, convenient staging areas, and multiple workflows.

* **Git official homepage** - [https://git-scm.com/](https://git-scm.com/)
* **Git cheat sheet** - [https://git-scm.com/](https://github.github.com/training-kit/downloads/github-git-cheat-sheet/)

---
Some of the basic operations in Git are:  
- Initialize  
- Add  
- Commit  
- Pull  
- Push  
Some advanced Git operations are:
- Branching  
- Merging  
- Rebasing  

![pic](images/lifecycle.png)
*Image taken from www.edureka.co*

## Author

* **Daniel Ocampo** - [email](daniel.ocampomillan@my.utsa.edu)

